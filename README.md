# 文件树分析器 (File Tree Analyzer)

## 项目介绍

文件树分析器是一个用Python开发的桌面应用程序，主要用于分析项目文件结构并生成markdown格式的文档。该工具特别适合以下场景：

- 项目文档生成
- 代码结构分析
- 项目文件结构可视化
- AI模型分析代码时的辅助工具

## 核心功能

1. **文件树生成**
   - 递归遍历目标文件夹
   - 生成层级化的文件树结构
   - 支持文件夹和文件的智能排除

2. **文件内容提取**
   - 自动读取文件内容
   - 支持UTF-8编码
   - 错误处理机制

3. **Markdown生成**
   - 标准化的markdown格式输出
   - 包含文件树结构和文件内容
   - 支持代码块格式化

## 使用说明

### 安装要求

- Python 3.6+
- tkinter (通常随Python一起安装)

### 运行方法

```bash
python file_tree_analyzer.py
```

### 打包方法

```bash
pip install pyinstaller
pyinstaller -F fd2md.py
```

### 使用步骤

1. **选择目标文件夹**
   - 点击"浏览"按钮选择要分析的文件夹
   - 或直接在输入框中输入文件夹路径

2. **配置排除规则**
   - 排除文件后缀：默认包含常见的二进制文件和临时文件后缀
   - 排除文件夹：默认包含常见的临时文件夹和构建目录

3. **开始分析**
   - 点击"开始分析"按钮
   - 等待进度条完成
   - 在预览区查看生成的markdown内容

4. **保存结果**
   - 点击"保存Markdown"按钮
   - 选择保存位置
   - 文件将以.md格式保存

### 默认排除规则

#### 排除文件后缀
```
.git, .pyc, .exe, .dll, .pyd, .so, .dylib, .cache, .png, .doc, .docx, .pdf, .bat, .sh, .txt, .gitignore, .json, .md
```

#### 排除文件夹
```
temp, tmp, .git, .idea, __pycache__, venv, .venv, node_modules, build, dist, .vscode, migrations, tests, logs, instance
```

## 技术实现

### 架构设计

1. **GUI层**
   - 使用tkinter构建用户界面
   - 采用面向对象的设计方式
   - 实现了主要的用户交互组件

2. **业务逻辑层**
   - 文件系统遍历
   - 内容读取和处理
   - Markdown格式化

3. **异步处理**
   - 使用threading实现异步操作
   - 防止界面卡顿
   - 实现进度反馈

### 核心类说明

#### FileTreeAnalyzer
主要类，继承自`tk.Tk`，包含以下关键方法：

- `create_control_area()`: 创建控制界面
- `create_status_area()`: 创建状态显示区域
- `create_preview_area()`: 创建预览区域
- `analyze_folder()`: 核心分析逻辑
- `save_markdown()`: 保存结果文件

### 错误处理

1. **文件读取错误**
   - 处理编码问题
   - 处理权限问题
   - 处理文件不存在情况

2. **用户输入验证**
   - 检查文件夹选择
   - 验证排除规则格式

## 最佳实践

1. **大型项目分析**
   - 建议先配置适当的排除规则
   - 关注进度条反馈
   - 预览确认后再保存

2. **定制化需求**
   - 可以修改默认排除规则
   - 根据项目特点调整文件处理逻辑

## 扩展建议

1. **功能扩展**
   - 添加文件大小统计
   - 支持更多文件编码
   - 添加文件类型统计

2. **性能优化**
   - 实现增量分析
   - 添加缓存机制
   - 优化大文件处理

## 注意事项

1. **性能考虑**
   - 处理大型项目时可能需要较长时间
   - 建议适当使用排除规则

2. **文件编码**
   - 默认使用UTF-8编码
   - 非UTF-8编码文件可能出现读取错误

3. **系统资源**
   - 大型项目分析可能占用较多内存
   - 建议关闭不必要的应用程序

## 贡献指南

欢迎贡献代码或提出建议：

1. Fork 项目
2. 创建功能分支
3. 提交更改
4. 推送到分支
5. 提交Pull Request

## 许可证

MIT License